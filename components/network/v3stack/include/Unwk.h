/**
******************************************************************************
 * @copyright Copyright (c) 2014-2020 UIOT Group. All rights reserved.
 *
 * @brief  提供网络层初始化接口
 * @file   Unwk.h
 * @date   8/10/2020
 * @author hfys
******************************************************************************
*/
#ifndef _UNWK_H_
#define _UNWK_H_

#include "UnwkTransmit.h"
#include "UnwkReceive.h"

/**
******************************************************************************
 * @brief     宏定义
******************************************************************************
*/
#define NWK_MAX_PAYLOAD 						98

#define TRANSMIT_TIMEOUTS_SLEEPTIME_INFINITE	0xFFFFFFFF
#define TRANSMIT_MAX_TIMEOUT					0x7FFFFFFF

/**< Check if timer's expiry time is greater than time and care about u32_t wraparounds*/
#define TIME_LESS_THAN(t, CompareTo) ( (((uint32)((t)-(CompareTo))) > TRANSMIT_MAX_TIMEOUT) ? 1 : 0 )


/**
 * @enum 节点类型
 */
typedef enum PACK
{
	E_UNWK_DEVICE_TYPE_COOR = 0,		/**< 协调器*/
    E_UNWK_DEVICE_TYPE_ROUTER = 1,		/**< 路由*/
    E_UNWK_DEVICE_TYPE_ENDDEVICE = 2,	/**< 终端*/
    E_UNWK_DEVICE_TYPE_UNKNOW,			/**< 未知*/
}teUnwkDeviceType;

/**
 * @enum 节点网络状态
 */
typedef enum PACK
{
	E_UNWK_NET_STATE_INIT = 1,			/**< 初始状态*/
	E_UNWK_NET_STATE_FIND = 2,			/**< 找网*/
    E_UNWK_NET_STATE_LEAVE = 3,			/**< 离网*/
    E_UNWK_NET_STATE_IN_NET = 4,		/**< 2 3 4是为了兼容V3模组*/
}teUnwkNetState;

/**
******************************************************************************
 * @brief     类型定义
******************************************************************************
*/
/**
 * @struct 节点信息
 */
typedef struct
{
	uint64 u64IeeeAddress;			/**< 节点长地址*/
	uint32 u32Address;				/**< 节点短地址*/
	
	teUnwkDeviceType  eDeviceType;	/**< 节点类型*/
	uint8  u8Channel;				/**< 节点信道*/
	uint16 u16PanId;				/**< 节点网络ID*/
	
	uint32 u32LastRecvSrcAddr;		/**< 节点接收最后一帧数据的源地址*/
	uint8  u8LastRecvSeq;			/**< 节点接收最后一帧数据的序号*/
	
	uint8  u8LostHeartBeat;			/**< 节点与中心是否失联标记*/
	teUnwkNetState eNetState;		/**< 节点网络状态*/
	
	uint8  u8LastRecvAckSeq;		/**< Ack seq*/
	uint32 u32LastRecvAckDestAddr;	/**< Ack destination addr*/
	
	uint16 u16Reserved;
}tsUnwkNodeInfo;

/**
 * @struct 应用层接收数据结构体
 */
typedef struct
{
	uint32 u32SrcAddress;				//数据源地址
	uint8  u8Rssi;						//信号强度
	uint8  u8Length;					//载荷长度
	uint8  au8Payload[NWK_MAX_PAYLOAD];	//载荷
}tsUnwkRcvPayload;

/**
 * @struct 联通状态监测
 */
typedef struct
{
    bool bLinked;                       //是否处于联通状态
}tsLinkStatusMonitor;

/**
 * @struct 节点找网络请求上报
 */
typedef struct
{
    uint32 u32TimeStamp;                //时间戳
    bool bEnable;                       //是否使能节点找网络请求上报
}tsReportFindCoorCmd;

/**
******************************************************************************
 * @brief     函数声明
******************************************************************************
*/
/**
******************************************************************************
 * @brief 		Nwk层初始化
 *
 * @param[in] 	eUnwkDeviceType	节点类型，在teUnwkDeviceType中定义
 * @param[in] 	bIsInNet		网络状态，TRUE:在网，FALSE:不在网
 * @param[in] 	bRxOn			接收机状态，TRUE:开启接收机，FALSE:关闭接收机
 * @param[in] 	bColdStart		TRUE 冷启动，FALSE 热启动
 *
 * @retval 		none
 * @see 		None.
 * @note 		None
******************************************************************************
*/
PUBLIC void vNwkInit( teUnwkDeviceType eUnwkDeviceType,
					  bool_t bIsInNet,
					  bool_t bRxOn,
					  bool_t bColdStart,
					  bool_t bCustonm,
					  uint8  u8Channel,
					  uint16 u16PanID );

/**
 * @brief       网络层运行
 * 
 * @param[in]   None
 * @param[out]  None
 * 
 * @retval      None
 * @see         None
 * @note        None
 */
PUBLIC void vNwkRun( void );

/**
 * @brief       测量并上报RSSI
 * 
 * @param[in]   None
 * @param[out]  None
 * 
 * @retval      None
 * @see         None
 * @note        None
 */
PUBLIC void vNwkMeasureAndReportRSSI( void );

#endif

/**
******************************************************************************
 * @brief     End of file
******************************************************************************
*/
