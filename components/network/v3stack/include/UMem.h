/**
 * @copyright Copyright (c) 2014-2022  UIOT Group. All rights reserved.
 * 
 * @brief     
 * @file      UMem.h
 * @date      2022-09-08
 * @author    cuiht(zgwl_cuiht@unisiot.com)
 * @version   1.0
 * 
 */


#ifndef _U_MEM_H_
#define _U_MEM_H_
#include "TypeDefine.h"

#define MEMSIZE         (32)
#define MEMBLOCKSIZE    (156)


/**
 * @brief     协议栈静态内存管理初始化
 * 
 * 
 * @see None
 * @note None
 */
void UMem_vInit(void);



/**
 * @brief     判断内存没有用完
 * 
 * 
 * @retval    BOOL:TRUE:有空闲；FALSE:没有空闲
 * @see None
 * @note None
 */
BOOL UMem_bJudgeIsNotFull(void);


/**
 * @brief     内存占用率
 * 
 * 
 * @retval    uint8:         
 * @see None
 * @note None
 */
uint8 UMem_u8Utilization(void);


/**
 * @brief     申请内存
 * 
 * 
 * @retval    void*:  返回内存地址       
 * @see None
 * @note None
 */
void *UMem_pvMalloc(void);


/**
 * @brief     释放内存
 * 
 * @param[in] pvBuf:  要释放的地址      
 * 
 * @retval    uint8:   错误原因      
 * @see None
 * @note None
 */
uint8 UMem_u8Free(void *pvBuf);




#endif





