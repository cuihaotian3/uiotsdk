/**
******************************************************************************
 * @copyright Copyright (c) 2014-2020 UIOT Group. All rights reserved.
 *
 * @brief  提供网络层数据发送接口
 * @file   UnwkTransmit.h
 * @date   8/8/2020
 * @author hfys
******************************************************************************
*/
#ifndef _UNWK_TRANSMIT_H_
#define _UNWK_TRANSMIT_H_

#include "Umac.h"

/**
******************************************************************************
 * @brief     类型定义
******************************************************************************
*/
/**
 * @struct 应用层发送单播数据结构体
 */
typedef struct
{
	uint32 u32DestAddr;				/**<目标地址*/

	// uint8 *pu8Data;					/**<payload数据指针*/
	uint8  pu8Data[73];				/**<payload数据*/
	uint8  u8Length;				/**<payload数据长度*/

	uint8  u8MagicNum;				/**<协议标签，用于区分应用层协议的用途*/

	uint8  u8NwkMaxTransNum;		/**<网络层最大传输次数*/
	uint8  u8MacReTransNum;			/**<MAC层重传次数*/
	uint8  u8MacCcaErrorReTransNum;	/**<遇到CCA错误时重传次数*/

	uint8  u8Reserved;				/**<保留字段*/
	uint16 u16NwkRetransInterval;	/**<网络层重传间隔*/
}tsUnwkUnicastReq;

/**
 * @struct 应用层发送广播数据结构体
 */
typedef struct
{
	// uint8 *pu8Data;					/**<payload数据指针*/
	uint8  pu8Data[73];				/**<payload数据*/
	uint8  u8Length;				/**<payload数据长度*/

	uint8  u8MagicNum;				/**<协议标签，用于区分应用层协议的用途*/

	uint8  u8NwkMaxTransNum;		/**<网络层最大传输次数*/
	uint8  u8MacReTransNum;			/**<MAC层重传次数*/
	uint8  u8MacCcaErrorReTransNum;	/**<遇到CCA错误时重传次数*/

	uint8  u8Reserved;				/**<保留字段*/
	uint16 u16NwkRetransInterval;	/**<网络层重传间隔*/
}tsUnwkBroadcastReq;

/**
******************************************************************************
 * @brief     函数声明
******************************************************************************
*/
/**
 * @brief Router和EndDevice发送“入网宣告”协议
 *
 * @param [in] u32SrcAddr  源地址
 * @param [in] u32DestAddr 目的地址
 *
 * @return uint8 UMAC层发送结果
 * @see None.
 * @note None
 */
PUBLIC uint8 u8UnwkDeviceAnnounce(uint32 u32SrcAddr,uint32 u32DestAddr,uint8 u8Rssi);

/**
******************************************************************************
 * @brief 		路由节点发送“找协调器”协议
 *
 * @param 		无
 *
 * @return 		uint8 UMAC层发送结果
 * @see 		None.
 * @note 		None
******************************************************************************
*/
PUBLIC uint8 u8UnwkFindCoorReq(void);

#endif

/**
******************************************************************************
 * @brief     End of file
******************************************************************************
*/
