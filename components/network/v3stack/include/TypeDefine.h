/**
 * @copyright Copyright (c) 2014-2022  UIOT Group. All rights reserved.
 * 
 * @brief     类型重定义文件
 * @file      TypeDefine.h
 * @date      2022-08-10
 * @author    cuiht(zgwl_cuiht@unisiot.com)
 * @version   1.0
 * 
 */

#ifndef _TYPE_DEFINE_H_
#define _TYPE_DEFINE_H_

#include <stdbool.h>
#include <stdint.h>

#define PACK      __attribute__ ((packed))        /* align to byte boundary  */

#if !defined PUBLIC
#define PUBLIC
#endif

#if !defined PRIVATE
#define PRIVATE static
#endif

#if !defined FALSE && !defined TRUE
#define TRUE            (1)   /* page 207 K+R 2nd Edition */
#define FALSE           (0)
#endif 

#define DBG_vPrintf blog_info_user

typedef uint8_t                 bool_t;

typedef unsigned char BOOL;

typedef int8_t                  int8;
typedef int16_t                 int16;
typedef int32_t                 int32;
typedef int64_t                 int64;
typedef uint8_t                 uint8;
typedef uint16_t                uint16;
typedef uint32_t                uint32;
typedef uint64_t                uint64;

typedef float                   f32;
typedef double                  f64;

typedef char *                  string;




#endif




