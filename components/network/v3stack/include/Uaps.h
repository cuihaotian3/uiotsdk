/**
******************************************************************************
 * @copyright Copyright (c) 2014-2020 UIOT Group. All rights reserved.
 * @brief  提供APS层重发函数接口
 * @file   Uaps.h
 * @date   8/18/2020
 * @author hfys
******************************************************************************
*/
#ifndef __UAPS_H__
#define __UAPS_H__

#include "Umac.h"
#include "UnwkTransmit.h"

/**
******************************************************************************
 * @brief     类型定义
******************************************************************************
*/
/**根据u8NwkReTransmit字段指定的发送次数执行发送，如果发送结束后，仍未收到回复，则通
 * 过该回调函数通知应用层。
 * 例如：
 * u8NwkReTransmit的值为3：
 * 1.如果实际发送的次数等于3，在发送结束后仍未收到回复，将调用该回调函数。
 * 2.如果实际发送的次数小于3（假设第1次发送后就收到了回复，会结束发送），则不调用该回调函数。
 * 
 * @param arg u8UapsUnicastDataReq接口传入的参数
 */
typedef void (*pvSendCompleteCallback)(void *arg);

/**
******************************************************************************
 * @brief     函数声明
******************************************************************************
*/
/**
******************************************************************************
 * @brief 		低功耗配置
 *
 * @param[in] 	bEnable TRUE:开启，FALSE:关闭
 * 
 * @retval 		无
 * @see 		None.
 * @note 		休眠类设备需调用该接口
******************************************************************************
*/
PUBLIC void vUapsConfigLowPower( BOOL bEnable );

/**
******************************************************************************
 * @brief 		应用层发送单播数据接口
 *
 * @param[in] 	sUnwkUnicastReq 	应用层数据
 * @param[in] 	u16StartTime 		发送延迟时间
 * @param[in] 	pvHandler    		发送结束回调函数
 * @param[in] 	arg 		   		发送结束回调函数参数
 
 * @retval 		执行结果
 * 				- V3_STACK_STATUS_SUCCESS 		插入链表成功
 * 				- V3_STACK_STATUS_NO_APDU 		没有可用的APDU
 * 				- V3_STACK_STATUS_LENGTH_ERROR  发送数据过长
 * @see 		None.
 * @note 		None.
******************************************************************************
 */
PUBLIC uint8 u8UapsUnicastDataReq( tsUnwkUnicastReq sUnwkUnicastReq,
								   uint16 u16StartTime,
								   pvSendCompleteCallback pvHandler,
								   void *arg );

/**
******************************************************************************
 * @brief 		应用层发送广播数据接口
 *
 * @param[in] 	sUnwkBroadcastReq 	应用层数据
 * @param[in] 	u16StartTime 		发送延迟时间
 * @param[in] 	pvHandler    		发送结束回调函数
 * @param[in] 	arg 		   		发送结束回调函数参数
 
 * @retval 		执行结果
 * 				- V3_STACK_STATUS_SUCCESS 		插入链表成功
 * 				- V3_STACK_STATUS_NO_APDU 		没有可用的APDU
 * 				- V3_STACK_STATUS_LENGTH_ERROR  发送数据过长
 * @see 		None.
 * @note 		None.
******************************************************************************
 */
PUBLIC uint8 u8UapsBroadcastDataReq( tsUnwkBroadcastReq sUnwkBroadcastReq,
								     uint16 u16StartTime,
								     pvSendCompleteCallback pvHandler,
								     void *arg );

/**
******************************************************************************
 * @brief 		与协议标签或协议载荷匹配的待发送数据帧将取消发送
 *
 * @param[in] 	u8NwkMagicNum	待取消协议的协议标签
 * @param[in] 	pu8Data			待取消协议的payload指针
 * 
 * @retval 		执行结果
 * 				- 0 取消成功
 * 				- 1 传输链表为空
 * 				- 2 没有找到符合条件的传输协议
 * @see 		None
 * @note 		None
******************************************************************************
*/
PUBLIC uint8 u8UapsCancelTransmitEvent( uint8 u8NwkMagicNum, uint8 *pu8Data );

/**
******************************************************************************
 * @brief 		所有待发送的数据帧都将被取消发送
 *
 * @param[in] 	None
 * 
 * @retval 		执行结果
 * 				- 0 取消成功
 * 				- 1 传输链表为空
 * @see 		None
 * @note 		None
******************************************************************************
*/
PUBLIC uint8 u8UapsCancelAllTransmit( void );


#endif

/**
******************************************************************************
 * @brief     End of file
******************************************************************************
*/
