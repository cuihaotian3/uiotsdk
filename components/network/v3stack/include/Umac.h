/*
******************************************************************************
 * Copyright (c) 2014-2020 UIOT Group. All rights reserved.
 *
 * @brief  实现了MAC层收发状态的管理
 * @file   Umac.h
 * @date   7/27/2020
 * @author hfys
******************************************************************************
*/
#ifndef _UMAC_H_
#define _UMAC_H_

#include "UmacQueueApi.h"

/**
******************************************************************************
 * @brief     宏定义
******************************************************************************
*/
#define OUTPUT_POWER_MAX      			10		/**<JN5169 输出最大功率*/
#define OUTPUT_POWER_MIN      			(-32)	/**<JN5169 输出最小功率*/
#define ABS_OUTPUT_POWER_MIN  			(32)	/**<JN5169 输出最小功率绝对值*/

#define UMAC_V3_PROTOCOL_BASE_LENGTH 	27
#define UMAC_V3_PROTOCOL_MIN_LENGTH  	29		/**<27个字节的固定格式+2个字节的校验*/
#define UMAC_V3_PROTOCOL_MAX_LENGTH  	127		/**<MMAC库支持的最大长度为127字节*/

//帧头，注意芯片平台的大小端，大端为0x7EFF，小端为0xFF7E
#define UMAC_V3_PROTOCOL_HEADER    	 	0xFF7E	/**<V3协议头*/
#define UMAC_V3_PROTOCOL_HEADER_H    	0x7E	/**<V3协议头高字节*/
#define UMAC_V3_PROTOCOL_HEADER_L    	0xFF	/**<V3协议头低字节*/

/**
******************************************************************************
 * @brief     类型定义
******************************************************************************
*/
/**
 * MAC层射频收发机状态管理
 */
typedef enum PACK
{
    E_UMAC_STATE_IDLE,		/**< 空闲状态*/
    E_UMAC_STATE_TX,		/**< 发送状态*/
    E_UMAC_STATE_TXACTIVE,	/**< 正在发送状态*/
    E_UMAC_STATE_RE_TX,		/**< MAC层重发*/
    E_UMAC_STATE_RXREADY,	/**< 等待接收状态*/
    E_UMAC_STATE_RX,		/**< 接收状态*/
}teUmacState;

/**
 * V3协议帧基
 */
typedef struct
{
	uint16 u16Header;			/**< 帧头*/
	uint16 u16PanId;			/**< 网络ID*/
	uint8  u8Protocol;			/**< 协议类型*/
	uint8  u8PadA;				/**< 未知字段*/
	uint8  u8Instruction;		/**< 指令类型*/
	uint8  u8Seq;				/**< 序号*/
	uint32 u32SrcAddr;			/**< 源地址*/
	
	uint32 u32DestAddr;			/**<目的地址 实际协议u32DestAddr 应该在 u8Direction的后面*/
	
	uint8  u8Direction;			/**<传输方向 他和目的地址的顺序颠倒一下 是 便于4字节对齐，方便CPU访问。
									目前这样定义sizeof这个结构体算出来的字节数是28，否则会计算得出32*/
	
	uint8  u8PadX1;				/**< 未知字段X1*/
	uint8  u8PadX2;				/**< 未知字段X2*/
	uint8  u8PadX3;				/**< 未知字段X3*/
	uint32 u32CurrentAddr;		/**< 当前地址*/
	uint8  u8PadY1;				/**< 未知字段Y1*/
	uint8  u8PadY2;				/**< 未知字段Y2*/
	uint8  u8PadY3;				/**< 未知字段Y3*/
	uint8  u8DataLen;			/**<非协议内字段*/

	/**<发送控制字段*/
	uint8  u8NwkReTransmit;		/**< 网络层重传次数*/
	uint8  u8MacReTransmit;		/**< MAC层重传次数 =0 表示不需要MAC重传 允许的最大值为15*/
	uint8  u8MacCcaReTransmit;	/**< MAC层由于CCA错误导致重传次数 =0表示不需要使用CCA 允许的最大值为15*/
	uint8  u8Reserved;			/**< 预留字段*/
}tsV3FrameBase;

/**
 * V3协议帧
 * 实际最大能传127-27-2=98字节
 * 当前该结构体占用空间大小为 132 Byte
 */
typedef struct
{
	tsV3FrameBase sV3FrameBase;	/**< 协议基*/
	uint8  au8ExternData[100];	/**< payload*/
}tsV3Frame;

/**
******************************************************************************
 * @brief     函数声明
******************************************************************************
*/
/**
******************************************************************************
 * @brief 		配置MAC层信道
 *
 * @param[in] 	u8InitChannel 信道
 *
 * @retval 		无
 * @see 		None.
 * @note 		None
******************************************************************************
*/
PUBLIC void vUmacConfigChannel( uint8 u8InitChannel );

/**
******************************************************************************
 * @brief 		配置PA
 *
 * @param[in] 	bEnable TRUE:开启PA，FALSE：关闭PA
 *
 * @retval 		无
 * @see 		None.
 * @note 		该函数要先于vUmacInit和vUmacConfigChannel被调用
******************************************************************************
*/
PUBLIC void vUmacConfigPA( BOOL bEnable );

/**
******************************************************************************
 * @brief 		MAC层初始化函数
 *
 * @param[in] 	u8InitChannel 	信道
 * @param[in] 	u8RxOn 			是否开启射频接收
 
 * @retval 		无
 * @see 		None.
 * @note 		None
******************************************************************************
*/
PUBLIC void vUmacInit( uint8 u8InitChannel, uint8 u8RxOn );

/**
******************************************************************************
 * @brief 		设置接收机状态
 *
 * @param[in] 	u8RxOn：TRUE：允许接收 FALSE：禁止接收
 *
 * @retval 		无
 * @see 		None.
 * @note 		None
******************************************************************************
*/
PUBLIC void vUmacSetRxWhenIdle(uint8 u8RxOn);

/**
******************************************************************************
 * @brief 		获取Mac地址
 *
 * @param[in] 	无
 *
 * @retval 		Mac地址
 * @see 		None.
 * @note 		None
******************************************************************************
*/
PUBLIC uint64 u64UmacGetMacAddr(void);


#endif

/**
******************************************************************************
 * @brief     End of file
******************************************************************************
*/
