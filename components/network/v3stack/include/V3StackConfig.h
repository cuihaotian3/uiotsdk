/**
******************************************************************************
 * @copyright Copyright (c) 2014-2020 UIOT Group. All rights reserved.
 *
 * @brief  V3协议栈配置头文件.
 * @file   V3StackConfig.h
 * @date   8/8/2020
 * @author hfys
******************************************************************************
*/
#ifndef _V3_STACK_CONFIG_H_
#define _V3_STACK_CONFIG_H_

#include "Unwk.h"
#include "Uaps.h"
/**
******************************************************************************
 * @brief     宏定义
******************************************************************************
*/
#define UMAC_SUPPORT_ONLY_V3

/**
 *该宏定义了重发中最后一条发出去多久后释放PDUM 并主动结束一个Activity
*如果该值定的很小的话，会因为Activity提前=0导致设备未发完数据就休眠
*在门窗磁产品中经过实际测试UMAC层需要6ms可以把心跳数据发完
*实际需要多久发完跟数据长度有关，也跟软件是否阻塞有关,在门窗磁应用中该值可以定义为7或8*/
#define URETRANS_FREE_PDUM_AFTER_MS   7

#define V3STACK_VERSION_H   0x07
#define V3STACK_VERSION_L   0x0B

/**
 *函数执行返回码
 */
//MAC层状态码
#define	UMAC_STATUS_SUCCESS			0x00	/**< 成功*/
#define	UMAC_STATUS_TX_LENGTH		0x01	/**< 长度不符合要求*/
#define UMAC_STATUS_TX_FIFO_FULL	0x02	/**< 无可用FIFO*/

//NWK层状态码
#define UNWK_STATUS_SUCCESS			0x00	/**<成功*/
#define UNWK_STATUS_NO_APDU			0x11
#define UNWK_STATUS_LENGTH_ERROR	0x12
#define	UNWK_STATUS_PARA_ERROR		0x13	/**< 参数错误*/
#define	UNWK_STATUS_HEADER_ERROR	0x14	/**< 帧头错误*/
#define	UNWK_STATUS_LINKLIST_EMPTY	0x15	/**< 链表为空*/
#define	UNWK_STATUS_NOT_FIND    	0x16	/**< 没有发现符合的内容*/

//APS层状态码
#define UAPS_STATUS_SUCCESS			0x00	/**<成功*/
#define UAPS_STATUS_LENGTH_ERROR	0x21	/**<长度出错*/
#define UAPS_STATUS_NO_APDU			0x22	/**<无可用APDU*/
#define	UAPS_STATUS_LINKLIST_EMPTY	0x23	/**< 链表为空*/
#define	UAPS_STATUS_NOT_FIND    	0x24	/**< 没有发现符合的内容*/
#define	UAPS_STATUS_LEAVE_NET    	0x25	/**< 处于离网状态*/

/**
******************************************************************************
 * @brief     类型定义
******************************************************************************
*/
typedef struct
{
	uint32 u32DestAddr;				/**<目标地址*/

	uint8  pu8Data[73];				/**<payload数据*/
	uint8  u8Length;				/**<payload数据长度*/

	uint8  u8MagicNum;				/**<协议标签，用于区分应用层协议的用途*/

	uint8  u8NwkMaxTransNum;		/**<网络层最大传输次数*/
	uint8  u8MacReTransNum;			/**<MAC层重传次数*/
	uint8  u8MacCcaErrorReTransNum;	/**<遇到CCA错误时重传次数*/

	uint8  u8Reserved;				/**<保留字段*/
	uint16 u16NwkRetransInterval;	/**<网络层重传间隔*/
}tsAppUnicastReq;

typedef struct
{
    tsUnwkUnicastReq sUnwkUnicastReq;
    uint16 u16StartTime;
    pvSendCompleteCallback pvHandler;
    void *arg ;
}tsRcvPayloadFromApp;

typedef struct
{
    tsUnwkBroadcastReq sUnwkBroadcastReq;
    uint16 u16StartTime;
    pvSendCompleteCallback pvHandler;
    void *arg ;
}tsRcvBroadcastPayloadFromApp;


typedef void (*CB_StackDataIndication)( tsUnwkRcvPayload *psData );
typedef BOOL (*CB_SendData)(tsUnwkRcvPayload * pvTxBuff);
typedef BOOL (*CB_ReceiveData)(tsRcvPayloadFromApp * pvTxBuff);
typedef BOOL (*CB_ReceiveBroadcastData)(tsRcvBroadcastPayloadFromApp * pvTxBuff);
/**
******************************************************************************
 * @brief     函数声明
******************************************************************************
*/
/**
******************************************************************************
 * @brief       注册协议栈回调函数，用以处理协议栈接收到的应用数据
 *
 * @param[in]   CB_StackDataIndication  回调函数
 *
 * @retval      None
 * @see         None
 * @note        None
******************************************************************************
*/
void vV3StackRegisterCallBack( CB_StackDataIndication pFunc );

/**
 * @brief     发送数据给应用层回调函数注册
 * 
 * @param[in] pfSendData:函数指针   
 * 
 * @see None
 * @note None
 */
void HAL_vSendDataRegisterCallBack(CB_SendData pfSendData);

/**
 * @brief     接收应用层的单播发送数据回调函数注册
 * 
 * @param[in] pfSendData:函数指针   
 * 
 * @see None
 * @note None
 */
void HAL_vReceiveDataRegisterCallBack(CB_ReceiveData pfReceiveData);

/**
 * @brief     接收应用层的组播发送数据回调函数注册
 * 
 * @param[in] pfSendData:函数指针   
 * 
 * @see None
 * @note None
 */
void HAL_vReceiveBroadcastDataRegisterCallBack(CB_ReceiveBroadcastData pfReceiveData);

/**
******************************************************************************
 * @brief       协议栈运行接口
 *
 * @param[in]   无
 *
 * @retval      无
 * @see         None.
 * @note        需要在mian函数的while(1)中循环调用该函数
******************************************************************************
*/
void vV3StackTask( void );


#endif

/**
******************************************************************************
 * @brief     End of file
******************************************************************************
*/
