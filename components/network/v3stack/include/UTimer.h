/****************************************************************************
 *
 * Copyright 2020 NXP.
 *
 * NXP Confidential. 
 * 
 * This software is owned or controlled by NXP and may only be used strictly 
 * in accordance with the applicable license terms.  
 * By expressly accepting such terms or by downloading, installing, activating 
 * and/or otherwise using the software, you are agreeing that you have read, 
 * and that you agree to comply with and are bound by, such license terms.  
 * If you do not agree to be bound by the applicable license terms, 
 * then you may not retain, install, activate or otherwise use the software. 
 * 
 *
 ****************************************************************************/


/*****************************************************************************
 *
 * MODULE:             UTimer
 *
 * COMPONENT:          UTimer.h
 *
 * DESCRIPTION:        Zigbee Timer Module
 *
 ****************************************************************************/

#ifndef UTIMER_H_
#define UTIMER_H_

#include "TypeDefine.h"

/****************************************************************************/
/***        Macro Definitions                                             ***/
/****************************************************************************/

#define UTIMER_TIME_SEC(v) ((uint32)(v) * 1000UL)
#define UTIMER_TIME_MSEC(v) ((uint32)(v) * 1UL)

/* Flags for timer configuration */
#define UTIMER_FLAG_ALLOW_SLEEP     0
#define UTIMER_FLAG_PREVENT_SLEEP   (1 << 0)

/****************************************************************************/
/***        Type Definitions                                              ***/
/****************************************************************************/
typedef enum
{
    E_UTIMER_STATE_CLOSED,
    E_UTIMER_STATE_STOPPED,
    E_UTIMER_STATE_RUNNING,
    E_UTIMER_STATE_EXPIRED,    
} UTIMER_teState;

typedef void (*UTIMER_tpfCallback)(void *pvParam);

typedef struct
{
    uint8               u8Flags;
    UTIMER_teState      eState;
    uint32                u32Time;
    void                *pvParameters;
    UTIMER_tpfCallback    pfCallback;
} UTIMER_tsTimer;

typedef enum
{
    E_UTIMER_OK,
    E_UTIMER_FAIL
} UTIMER_teStatus;

/****************************************************************************/
/***        Exported Functions                                            ***/
/****************************************************************************/

PUBLIC UTIMER_teStatus UTIMER_eInit(UTIMER_tsTimer *psTimers, uint8 u8NumTimers);
PUBLIC void UTIMER_vAhiCallback ( uint32 u32Device, uint32 u32ItemBitmap);
PUBLIC void UTIMER_vSleep(void);
PUBLIC void UTIMER_vWake(void);
PUBLIC void UTIMER_vTask(void);
PUBLIC UTIMER_teStatus UTIMER_eOpen(uint8 *pu8TimerIndex, UTIMER_tpfCallback pfCallback, void *pvParams, uint8 u8Flags);
PUBLIC UTIMER_teStatus UTIMER_eClose(uint8 u8TimerIndex);
PUBLIC UTIMER_teStatus UTIMER_eStart(uint8 u8TimerIndex, uint32 u32Time);
PUBLIC UTIMER_teStatus UTIMER_eStop(uint8 u8TimerIndex);
PUBLIC UTIMER_teState UTIMER_eGetState(uint8 u8TimerIndex);
PUBLIC void UTIMER_vStopAllTimers(void);
/****************************************************************************/
/***        External Variables                                            ***/
/****************************************************************************/

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

#endif /*UTIMER_H_*/
