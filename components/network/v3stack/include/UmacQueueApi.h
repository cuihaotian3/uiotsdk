/**
 * @copyright Copyright (c) 2014-2020 UIOT Group. All rights reserved.
 *
 * @brief  提供FIFO的申请 写入 读取 释放接口
 *
 * @file   UmacQueueApi.h
 * @date   8/10/2020
 * @author hfys
 */

#ifndef  _APP_MAC_QUEUE_API_H_
#define  _APP_MAC_QUEUE_API_H_

#if defined __cplusplus
extern "C" {
#endif

#include "TypeDefine.h"
#include "Halmac.h"

/**
 * @enum 节点类型
 * Each FIFO queue has a pointer to the current read position and write
 * position, and the start and end of the queue
 */
typedef struct
{
    void **ppvReadPtr;	
    void **ppvWritePtr;	
    void **ppvQueueStart;
    void **ppvQueueEnd;	
    uint16 u16QueueUsed;
    uint16 u16QueueTotal;
} tsSFqueue;


/**
 * @brief MAC层 Tx Rx FIFO初始化.
 *
 * @param[in] 无
 *
 * @retval None
 * @see None
 * @note None
 */
PUBLIC void vAppQApiInit(void);

/**
 * @brief 申请Rx FIFO buffer.
 *
 * @param[in] 无
 *
 * @return 指向tsHalPhyFrame结构体的指针
 * @retval 
 * - NULL 未申请到
 * - 非空 申请到FIFO的地址
 * @see None
 * @note None
 */
PUBLIC tsHalPhyFrame* psAppQApiGetRxBuffer(void);

/**
 * @brief 申请Tx FIFO buffer.
 *
 * @param[in] 无
 *
 * @return 指向tsHalPhyFrame结构体的指针
 * @retval 
 * - NULL 未申请到
 * - 非空 申请到FIFO的地址
 * @see None
 * @note None
 */
PUBLIC tsHalPhyFrame* psAppQApiGetTxBuffer(void);
/**
 * @brief 向Rx FIFO buffer写入数据.
 *
 * @param[in] psPhyFrame
 *
 * @return 无
 * @see None
 * @note None
 */
PUBLIC void vAppQApiPostRxData(tsHalPhyFrame *psPhyFrame);

/**
 * @brief 向Tx FIFO buffer写入数据.
 *
 * @param[in] psPhyFrame
 *
 * @return 无
 * @see None
 * @note None
 */
PUBLIC void vAppQApiPostTxData(tsHalPhyFrame *psPhyFrame);

/**
 * @brief 从Rx FIFO buffer读出数据.
 *
 * @param[in] 无
 *
 * @return 指向psPhyFrame的指针
 * @retval 
 * - NULL 无可用数据
 * - 非空 读出数据的指针
 * @see None
 * @note None
 */
PUBLIC tsHalPhyFrame *psAppQApiReadRxData(void);

/**
 * @brief 从Tx FIFO buffer读出数据.
 *
 * @param[in] 无
 *
 * @return 指向psPhyFrame的指针
 * @retval 
 * - NULL 无可用数据
 * - 非空 读出数据的指针
 * @see None
 * @note None
 */
PUBLIC tsHalPhyFrame *psAppQApiReadTxData(void);

/**
 * @brief 释放Rx FIFO buffer.
 *
 * @param[in] psBuffer 待释放FIFO的地址
 *
 * @return 无
 * @see None
 * @note None
 */
PUBLIC void vAppQApiReturnRxBuffer(tsHalPhyFrame *psBuffer);

/**
 * @brief 释放Tx FIFO buffer.
 *
 * @param[in] psBuffer 待释放FIFO的地址
 *
 * @return 无
 * @see None
 * @note None
 */
PUBLIC void vAppQApiReturnTxBuffer(tsHalPhyFrame *psBuffer);

/**
 * @brief 获取Tx FIFO空间的使用情况.
 *
 * @param[in] 无
 *
 * @return uint16 Tx FIFO的耗用量
 * @see None
 * @note None
 */
PUBLIC uint16 u16AppQApiGetTxBufUsed(void);

/**
 * @brief 获取Rx FIFO空间的使用情况.
 *
 * @param[in] 无
 *
 * @return uint16 Rx FIFO的耗用量
 * @see None
 * @note None
 */
PUBLIC uint16 u16AppQApiGetRxBufUsed(void);

/**
 * @brief 释放所有Tx FIFO.
 *
 * @param[in] 无
 *
 * @return 无
 * @see None
 * @note None
 */
PUBLIC void vAppQApiFlushTxData(void);

/**
 * @brief 释放所有Rx FIFO.
 *
 * @param[in] 无
 *
 * @return 无
 * @see None
 * @note None
 */
PUBLIC void vAppQApiFlushRxData(void);

/****************************************************************************/
/***        Exported Variables                                            ***/
/****************************************************************************/

#if defined __cplusplus
}
#endif

#endif

/****************************************************************************/
/***        END OF FILE                                                   ***/
/****************************************************************************/

