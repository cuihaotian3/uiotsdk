#ifndef ZB_MANUF_UNISIOT_H
#define ZB_MANUF_UNISIOT_H

#include "zcl_common.h"


/******************************Basic Cluster Extension******************************/
// service side attribute
enum {
    ZCL_MANUF_ATTR_UNISIOT_BASIC_HEARTBEAT                      = 0x8001,   //data type: 0x41
    ZCL_MANUF_ATTR_UNISIOT_BASIC_INDICATION                     = 0x8002,   //data type: 0x10
    ZCL_MANUF_ATTR_UNISIOT_BASIC_BACKLIGHT                      = 0x8003,   //data type: 0x20
    ZCL_MANUF_ATTR_UNISIOT_BASIC_MIX_PANEL_SPEC_CFG             = 0x8004,   //data type: 0x41
    ZCL_MANUF_ATTR_UNISIOT_BASIC_SCREEN_COLOUR                  = 0x8005,   //data type: 0x23
    ZCL_MANUF_ATTR_UNISIOT_BASIC_SCREEN_BRIGHTNESS_LEVEL        = 0x8006,   //data type: 0x20
    ZCL_MANUF_ATTR_UNISIOT_BASIC_PRODUCT_SN                     = 0x8007,   //data type: 0x41
};


/******************************OnOff Cluster Extension******************************/
// service side attribute
enum {
    ZCL_MANUF_ATTR_UNISIOT_ONOFF_CHILD_LOCK                      = 0x8000,   //data type: 0x10
    ZCL_MANUF_ATTR_UNISIOT_ONOFF_KEY_BACK_LIGHT_MODE             = 0x8001,   //data type: 0x20
    ZCL_MANUF_ATTR_UNISIOT_ONOFF_STATE_RESTORE                   = 0x8002,   //data type: 0x10
    ZCL_MANUF_ATTR_UNISIOT_ONOFF_RELAY_ACTION_DELAY              = 0x8003,   //data type: 0x21
    ZCL_MANUF_ATTR_UNISIOT_ONOFF_SMART_LAMPS_BIND                = 0x8004,   //data type: 0x10
    ZCL_MANUF_ATTR_UNISIOT_ONOFF_SMART_LAMPS_BIND_ONOFF          = 0x8005,   //data type: 0x41
};


/******************************Power Configuration Cluster Extension******************************/
// service side attribute
enum {
    ZCL_MANUF_ATTR_UNISIOT_POWERCONFIGURATION_BATTERY_EXTENDED_ADDRESS   = 0x8007,   //data type: 0x10
};

/******************************Window Covering Cluster Extension******************************/
// service side attribute
enum {
    ZCL_MANUF_ATTR_UNISIOT_WINDOW_COVERING_STATE_RESTORE        = 0x8002,   //data type: 0x10
    ZCL_MANUF_ATTR_UNISIOT_WINDOW_COVERING_RELAY_ACTION_DELAY   = 0x8003,
};

/******************************IAS Zone Cluster Extension******************************/
// client side attribute
enum {
    ZCL_MANUF_ATTR_UNISIOT_IASZONE_CURTAIN_DIRECTION     = 0x001F,   //data type: 0x10
    ZCL_MANUF_ATTR_UNISIOT_IASZONE_ALARM_CONCENTRATION   = 0x8001,   //data type: 0x20
    ZCL_MANUF_ATTR_UNISIOT_IASZONE_TEST_RESULT           = 0x8002,   //data type: 0x20
    ZCL_MANUF_ATTR_UNISIOT_IASZONE_SELF_SCLENCE          = 0x8003,   //data type: 0x10
    ZCL_MANUF_ATTR_UNISIOT_IASZONE_LIFE                  = 0x8004,   //data type: 0x20
    ZCL_MANUF_ATTR_UNISIOT_IASZONE_PREHEAT               = 0x8005,   //data type: 0x10
    ZCL_MANUF_ATTR_UNISIOT_IASZONE_TIME                  = 0x8006,   //data type: 0x27
    ZCL_MANUF_ATTR_UNISIOT_IASZONE_ACTION_RESULT         = 0x8007,   //data type: 0x23
    ZCL_MANUF_ATTR_UNISIOT_IASZONE_BAND_LOAD             = 0x8008,   //data type: 0x20
};


/******************************Private Cluster Extension******************************/
// cluster id
enum {
    ZCL_MANUF_CLUST_UNISIOT_PASS_THROUGH             = 0x06A0,
    ZCL_MANUF_CLUST_UNISIOT_CERTIFICATION            = 0xFE00,
    ZCL_MANUF_CLUST_UNISIOT_SCENE                    = 0xFE05,
};


/******************************PASS THROUGH Cluster Extension******************************/
// client side attribute
enum {
    ZCL_MANUF_ATTR_UNISIOT_PASS_THROUGH_VERSION          = 0x0000,   //data type: 0x10
    ZCL_MANUF_ATTR_UNISIOT_PASS_THROUGH_USER_CUST_DATA   = 0x0001,   //data type: 0x20
};


/******************************CERTIFICATION Cluster Extension******************************/
// client side attribute
enum {
    ZCL_MANUF_ATTR_UNISIOT_CERTIFICATION_ODM_ID          = 0x0020,   //data type: 0x23
    ZCL_MANUF_ATTR_UNISIOT_CERTIFICATION_ODM_SIGN        = 0x0021,   //data type: 0x41
    ZCL_MANUF_ATTR_UNISIOT_CERTIFICATION_ODM_SIGN_RSP    = 0x0022,   //data type: 0x41
};


/******************************SCENE Cluster Extension******************************/
// client side attribute
enum {
    ZCL_MANUF_ATTR_UNISIOT_SCENE_KEYFOB          = 0x0000,   //data type: 0x20
    ZCL_MANUF_ATTR_UNISIOT_SCENE_CONFIG          = 0x0001,   //data type: 0x41
};

#define MAX_LEN_ZCL_BASIC_ATTR_HEART_BEAT                     16
#define MAX_LEN_ZCL_BASIC_ATTR_MIX_PANEL_SPEC_CFG             64
#define MAX_LEN_ZCL_BASIC_ATTR_PRODUCT_SN                     16
#define MAX_LEN_ZCL_ONOFF_ATTR_SMART_LAMPS_BIND_ONOFF         8

#define MAX_LEN_ZCL_MANUF_ATTR_UNISIOT_USER_CUST_DATA         64

#define MAX_LEN_ZCL_MANUF_ATTR_UNISIOT_CERTIFICATION_ODM_SIGN  16
#define MAX_LEN_ZCL_MANUF_ATTR_UNISIOT_CERTIFICATION_ODM_SIGN_RSP  16
#define MAX_LEN_ZCL_MANUF_ATTR_UNISIOT_SCENE_CONFIG               16

zbRet_t zcl_unisiotPassThroughRegisterServer(uint8_t ep);
zbRet_t zcl_unisiotCertificationRegisterServer(uint8_t ep);
zbRet_t zcl_unisiotSceneRegisterServer(uint8_t ep);



#endif
