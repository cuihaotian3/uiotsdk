#ifndef  __ZB_NOTIFY__
#define  __ZB_NOTIFY__

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include "zb_common.h"
#include "zcl_common.h"


#define CODE_USER_ZB_OTA_SERVER_SEARCH      1
#define CODE_USER_ZB_OTA_QUERY_NEXT_IMGE    2


zbRet_t zb_eventHandler(uint8_t evtId, uint8_t * evtParam);

zbRet_t zcl_onoffEventHandler(uint8_t ep, uint8_t evtId, uint8_t * evtParam);
zbRet_t zcl_levelControlEventHandler(uint8_t ep, uint8_t evtId, uint8_t * evtParam);
zbRet_t zcl_IdentifyEventHandler(uint8_t ep, uint8_t evtId, uint8_t * evtParam);


void register_zb_cb(void);
typedef void (*zb_appCb)(const uint8_t status, const uint8_t roleType);
void zb_register_app_cb(zb_appCb cb);

void zb_app_startup(void);
void zb_searchOtaServer(void);
void zcl_otaQueryNextImage(void);

void zb_pwm_operations_in_freertos(uint8_t color_mode, uint16_t status_hold_time_in_ms, uint8_t next_status);

#endif // __ZB_NOTIFY__
