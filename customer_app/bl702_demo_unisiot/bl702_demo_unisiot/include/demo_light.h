#ifndef UNISIOT_DEMO_LIGHT
#define UTISIOT_DEMO_LIGHT

#include <stdio.h>
#include <hosal_pwm.h>
#include <hosal_gpio.h>

typedef struct lightEP
{
    uint8_t  ep;                               // zb ep
    uint8_t switchGPIO;                           // switch button

    uint8_t backlightGPIO;                         //light load,  onoff demo
    hosal_gpio_dev_t backlight;

    uint8_t loadGPIO;                         //light load,  onoff demo
    hosal_gpio_dev_t load;

} lightEP;

void  demo_light_init(void);
void  demo_load_on(lightEP *ep);
void  demo_load_off(lightEP *ep);
void  demo_load_turnover(lightEP *ep, uint8_t  *value);
uint8_t  demo_load_status(lightEP *ep);


void demo_backlight_on(lightEP *ep);
void demo_backlight_off(lightEP *ep);

#endif

