/**
 * Copyright (c) 2016-2021 Bouffalolab Co., Ltd.
 *
 * Contact information:
 * web site:    https://www.bouffalolab.com/
 */

#include <stdio.h>
#include <cli.h>
#include <hosal_pwm.h>
#include <blog.h>

#include <bl_gpio.h>
#include <bl702_glb.h>
#include <bl702_gpio.h>
#include <hosal_gpio.h>

#include "demo_light.h"

#define EP1_SWITCH_PIN 16                   // switch for ep1; configure by dts
#define EP1_BACKLIGHT_PIN 30                      // pwm load for ep1; output gpio; pwm  
#define EP1_LOAD_PIN 31                     // onoff load for ep1; output gpio, hi/lo 

               
lightEP ep1;


void demo_light_init(void)
{
    printf("%s \r\n", __func__);
    ep1.ep = 1;

    ep1.switchGPIO = EP1_SWITCH_PIN;
    ep1.backlightGPIO = EP1_BACKLIGHT_PIN;
    ep1.loadGPIO = EP1_LOAD_PIN;

    /* gpio for backlight */
    ep1.backlight.port = EP1_BACKLIGHT_PIN;
    ep1.backlight.config = OUTPUT_OPEN_DRAIN_NO_PULL;
    hosal_gpio_init(&ep1.backlight);
    hosal_gpio_output_set(&ep1.backlight, 0);

    /* gpio for light load */ 
    ep1.load.port = EP1_LOAD_PIN;
    ep1.load.config = OUTPUT_OPEN_DRAIN_NO_PULL;
    hosal_gpio_init(&ep1.load);
    hosal_gpio_output_set(&ep1.load, 0);

}

/* backlight  on */
void demo_backlight_on(lightEP *ep){
    hosal_gpio_output_set(&(ep->backlight), 1);
}

/* backlight off */
void  demo_backlight_off(lightEP *ep)
{
    hosal_gpio_output_set(&(ep->backlight), 0);
}


/* light load on */
void  demo_load_on(lightEP *ep)
{
    hosal_gpio_output_set(&(ep->load), 1);
}

void  demo_load_off(lightEP *ep)
{
    /* light load off */
    hosal_gpio_output_set(&(ep->load), 0);

}

void demo_load_turnover(lightEP *ep, uint8_t *value)
{
    uint8_t gpioStatus = demo_load_status(ep);
    
    if(gpioStatus){
        *value = 0;
        demo_load_off(ep);
    }else{
        *value = 1;
        demo_load_on(ep);
    }

}

uint8_t demo_load_status(lightEP *ep)
{
    GLB_GPIO_Type gpioPin = ep->loadGPIO;

    if(gpioPin != ep1.loadGPIO){
        return 0xFF;
    }

    uint32_t *p = (uint32_t *)(GLB_BASE + GLB_GPIO_OUTPUT_OFFSET + ((gpioPin >> 5) << 2));
    uint32_t pos = gpioPin % 32;

    if ((*p) & (1 << pos)) {
        return 1;
    } else {
        return 0;
    }

}
