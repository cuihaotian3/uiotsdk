/*
 * Copyright (c) 2020 Bouffalolab.
 *
 * This file is part of
 *     *** Bouffalolab Software Dev Kit ***
 *      (see www.bouffalolab.com).
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 *   3. Neither the name of Bouffalo Lab nor the names of its contributors
 *      may be used to endorse or promote products derived from this software
 *      without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include <FreeRTOS.h>
#include <task.h>
#include <timers.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>

#include <vfs.h>
#include <device/vfs_uart.h>
#include <aos/kernel.h>
#include <aos/yloop.h>
#include <event_device.h>


#include <bl_sys.h>
#include <bl_chip.h>
#include <bl_wireless.h>
#include <bl_irq.h>
#include <bl_sec.h>
#include <bl_rtc.h>
#include <bl_uart.h>
#include <bl_gpio.h>
#include <bl_flash.h>
#include <bl_timer.h>
#include <bl_wdt.h>
#include <hal_boot2.h>
#include <hal_board.h>
#include <hosal_uart.h>
#include <hosal_gpio.h>
#include <hal_gpio.h>
#include <hal_button.h>
#include <hal_hwtimer.h>
#include <hal_pds.h>
#include <hal_tcal.h>
#include <FreeRTOS.h>
#include <timers.h>

#include <libfdt.h>
#include <utils_log.h>
#include <blog.h>

#ifdef EASYFLASH_ENABLE
#include <easyflash.h>
#endif
#include <utils_string.h>


#if defined(CFG_ZIGBEE_ENABLE)
#include "zb_common.h"
#include "zigbee_app.h"
//#include "zb_bdb.h"
#endif

//#include "bl_flash.h"
#include "demo_light.h"

void vApplicationMallocFailedHook(void)
{
    printf("Memory Allocate Failed. Current left size is %d bytes\r\n"
        ,xPortGetFreeHeapSize()
    );
    while (1) {
        /*empty here*/
    }
}

void vApplicationIdleHook(void)
{
    bl_wdt_feed();
    bool bWFI_disable =  false;
    if(!bWFI_disable){
        __asm volatile(
                "   wfi     "
        );
        /*empty*/
    }
}

#if ( configUSE_TICK_HOOK != 0 )
void vApplicationTickHook( void )
{
#if defined(CFG_USB_CDC_ENABLE)
    extern void usb_cdc_monitor(void);
    usb_cdc_monitor();
#endif
#if defined(CFG_ZIGBEE_ENABLE)
	extern void ZB_MONITOR(void);
	ZB_MONITOR();
#endif
}
#endif

void vApplicationGetIdleTaskMemory(StaticTask_t **ppxIdleTaskTCBBuffer, StackType_t **ppxIdleTaskStackBuffer, uint32_t *pulIdleTaskStackSize)
{
    /* If the buffers to be provided to the Idle task are declared inside this
    function then they must be declared static - otherwise they will be allocated on
    the stack and so not exists after this function exits. */
    static StaticTask_t xIdleTaskTCB;
    //static StackType_t uxIdleTaskStack[ configMINIMAL_STACK_SIZE ];
    static StackType_t uxIdleTaskStack[256];

    /* Pass out a pointer to the StaticTask_t structure in which the Idle task's
    state will be stored. */
    *ppxIdleTaskTCBBuffer = &xIdleTaskTCB;

    /* Pass out the array that will be used as the Idle task's stack. */
    *ppxIdleTaskStackBuffer = uxIdleTaskStack;

    /* Pass out the size of the array pointed to by *ppxIdleTaskStackBuffer.
    Note that, as the array is necessarily of type StackType_t,
    configMINIMAL_STACK_SIZE is specified in words, not bytes. */
    //*pulIdleTaskStackSize = configMINIMAL_STACK_SIZE; 
    *pulIdleTaskStackSize = 256;//size 256 words is For ble pds mode, otherwise stack overflow of idle task will happen.
}


#if defined(CFG_ZIGBEE_ENABLE)
void zigbee_init(void)
{
    zbRet_t status;
    status = zb_stackInit();
    if (status != ZB_SUCC)
    {
        printf("BL Zbstack Init fail : 0x%08x\r\n", status);
        //ASSERT(false);
    }
    else
    {
        printf("BL Zbstack Init Success\r\n");
    }

    register_zb_cb();
    
    zb_app_startup();

}
#endif

void event_cb_key_event(input_event_t *event, void *private_data)
{
    printf("%s privateData %ld \r\n", __func__, event->value);

    extern lightEP ep1;

    switch (event->code) {
        case KEY_1:
        {
            printf("[KEY_1] [EVT] INIT DONE %lld\r\n", aos_now_ms());
            printf("short press \r\n");
            

            if(event->value != ep1.switchGPIO){
                return;
            }

            /* switch on/off */
           if(zb_isJoined()){

                uint8_t onoffLoadStatus = 0 ;
                if(!demo_load_status(&ep1)){
                    onoffLoadStatus = 1;
                }

                /* set onoff attribute, onoff event handler will trigger the light turn over */
                extern zbRet_t zcl_setAttr(uint16_t clustId, uint16_t attrId, uint8_t * data, uint8_t size, uint8_t ep);
                zcl_setAttr(0x0006, 0x0000, &onoffLoadStatus, 1, ep1.ep); 
                
           }else{
               uint8_t onoffLoadStatus = 0 ;
               /* turn over load only */
               demo_load_turnover(&ep1, &onoffLoadStatus);
           }

        }
        break;
        case KEY_2:
        {
            printf("[KEY_2] [EVT] INIT DONE %lld\r\n", aos_now_ms());
            printf("long press \r\n");
            
            if(event->value == ep1.switchGPIO){
                extern void zb_app_scan_trigger();
                zb_app_scan_trigger();
            }
        }
        break;
        case KEY_3:
        {
            printf("[KEY_3] [EVT] INIT DONE %lld\r\n", aos_now_ms());
            printf("longlong press \r\n");
            if(event->value == ep1.switchGPIO){
                extern void  zb_app_leave();
                if (zb_isJoined()){
                    zb_app_leave();
                }
            }
        }
        break;
        default:
        {
            printf("[KEY] [EVT] Unknown code %u, %lld\r\n", event->code, aos_now_ms());
            /*nothing*/
        }
    }
}

void _dump_lib_info(void)
{
#if defined(CFG_ZIGBEE_ENABLE)
    puts("Zigbee LIB Version: ");
    puts(zb_getLibVer());
    puts("\r\n");
#endif
}


static void system_init(void)
{
    bl_rtc_init();

    hal_tcal_init();

    //bl_wdt_init(4000);

}

static void system_thread_init()
{

    uint32_t fdt = 0, offset = 0;

    if (0 == hal_board_get_dts_addr("gpio", &fdt, &offset)) {
        hal_gpio_init_from_dts(fdt, offset);
        fdt_button_module_init((const void *)fdt, (int)offset);
    }

    aos_register_event_filter(EV_KEY, event_cb_key_event, NULL);

    extern void demo_light_init(void);
    demo_light_init();

#if defined(CFG_ZIGBEE_ENABLE)
    zigbee_init();

    extern void zb_app_light_restore(void);
    zb_app_light_restore();

    #if defined(CONFIG_HW_SEC_ENG_DISABLE)
    //if sec engine is disabled, use software rand in bl_rand
    int seed = bl_timer_get_current_time();
    srand(seed);
    #endif
#endif

#ifdef EASYFLASH_ENABLE
    extern void reboot_check(void);
    reboot_check();
#endif
}

void main()
{
    system_init();
    system_thread_init();
}
