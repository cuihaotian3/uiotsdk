/*
 * Copyright (c) 2020 Bouffalolab.
 *
 * This file is part of
 *     *** Bouffalolab Software Dev Kit ***
 *      (see www.bouffalolab.com).
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 *   3. Neither the name of Bouffalo Lab nor the names of its contributors
 *      may be used to endorse or promote products derived from this software
 *      without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
//#if defined(CFG_ZIGBEE_ENABLE)
#include <FreeRTOS.h>
#include <semphr.h>
#include <timers.h>
#include <aos/yloop.h>
#include <hal_hwtimer.h>
#include <hal_gpio.h>
#include <hal_sys.h>
#include "bl702_glb.h"
#include "bl_sec.h"
#include "hal_boot2.h"
#include "hosal_ota.h"
#include "zigbee_app.h"
#include "zb_nwk.h"
#include "zb_zdp.h"
#include "zb_aps.h"
#include "zb_common.h"
#include "zcl_common.h"
#include "zcl_basic.h"
#include "zcl_onOff.h"
#include "zcl_level.h"
#include "zcl_identify.h"
#include "zcl_color.h"
#include "zcl_ota.h"
#include "zcl_groups.h"
#include "zcl_scenes.h"
#include "zcl_touchlink.h"
#include "zcl_iasZone.h"
#include "zcl_powerCfg.h"
#include "zcl_windowCovering.h"

#include "zb_unisiot.h"

#include "demo_light.h"

#ifdef EASYFLASH_ENABLE
#include <easyflash.h>
#endif

zb_appCb appCb;
uint8_t otaClientEp = 0xff;
TimerHandle_t otaTimerHdl;


TimerHandle_t zbStartTimerHdl;        // start joining one time with random delay
TimerHandle_t zbStartBreathTimerHdl;  // backlight breath during joining process
TimerHandle_t zbStartFinishTimerHdl;  // End the joining nwk process

TimerHandle_t deviceRebootCheckHdl;

TimerHandle_t ep1ReportTimerHdl;


#define ZCL_OTA_GET_PROGRESS_INTVL_IN_SEC 10
#define ZB_START_INTVL_IN_SEC 4
#define REBOOT_CHECK_IN_SEC 3


#define ATTR_REPORT_MAX_INTVL 6*60
#define ATTR_REPORT_MIN_INTVL 0

#define ZED_START_FINISH_IN_SEC 60

#define OTA_FILE_VERSION  0x00000001
#define OTA_IMAGE_TYPE 0x0000


extern lightEP ep1;

volatile static bool zb_has_joined_before = true;
volatile static bool zb_is_joining = false;

void zb_app_load_on(lightEP *ep)
{
    if(!ep)
        return;
    
    uint8_t uiotHBStr[] = {2, 0x00, 0x01};
    zcl_setAttr(ZCL_CLUST_BASIC, ZCL_MANUF_ATTR_UNISIOT_BASIC_HEARTBEAT, uiotHBStr, sizeof(uiotHBStr), ep->ep);

    demo_load_on(ep);
}

void zb_app_load_off(lightEP *ep)
{
    if(!ep)
        return;
    
    uint8_t uiotHBStr[] = {2, 0x00, 0x00};
    zcl_setAttr(ZCL_CLUST_BASIC, ZCL_MANUF_ATTR_UNISIOT_BASIC_HEARTBEAT, uiotHBStr, sizeof(uiotHBStr), ep->ep);

    demo_load_off(ep);
}


void zb_app_light_restore(void)
{
    if(zb_isJoined()){
        uint8_t onoff;
        uint8_t size;

        demo_backlight_on(&ep1);

        zcl_getAttr(ZCL_CLUST_ONOFF, ZCL_ATTR_ONOFF_ONOFF, &onoff, &size, ep1.ep);    

        if(onoff){
            zb_app_load_on(&ep1);
        }else{
            zb_app_load_off(&ep1);
        }

    }

}

void manually_send_a_report_online_to_zc(TimerHandle_t p_timerhdl)
{
    uint8_t reportEp;
 
    if(p_timerhdl == ep1ReportTimerHdl){
        reportEp = ep1.ep;

        ep1ReportTimerHdl = 0;

    }else{
        printf("%s invalid ep \r\n", __func__);
        return;
    }

    printf("%s \r\n", __func__);
    xTimerDelete(p_timerhdl, 0);

    struct _zclTxParam glb_zclTxParam;
    glb_zclTxParam.radius = 0;
    glb_zclTxParam.dstAddrMode = ZB_APS_ADDR_MODE_SHORT;
    glb_zclTxParam.dstAddr.shortAddr = 0000;
    glb_zclTxParam.srcEp = reportEp;
    glb_zclTxParam.dstEp = 1;
    glb_zclTxParam.profileId = ZB_PROFILE_ID_HA;
    glb_zclTxParam.txOptions = ZB_APS_TX_OPTIONS_ACK_TRANS;
    glb_zclTxParam.dir = 1;
    glb_zclTxParam.disZclDefaultResp = true;
    glb_zclTxParam.manufactureSpecific = false;


    uint8_t lightLoadStatus;
    uint8_t lightLoadSize;
    zcl_getAttr(ZCL_CLUST_ONOFF, ZCL_ATTR_ONOFF_ONOFF, &lightLoadStatus,  &lightLoadSize, ep1.ep);

    char uiotNameStr[13] = {12, 'U', 'I', 'O', 'T', '-', '[', '0', '2', ',', '0', '1',']'};
    char uiotDateStr[17] = {16, 'V', '4', '.', '8', '0', '-', '8', '.', '0', '0', '-','V','5','.','0', '0'};
    char uiotHBStr[3];

    if(lightLoadStatus){
        uiotHBStr[0] = 0x02;
        uiotHBStr[1] = 0x00;
        uiotHBStr[2] = 0x01;
    }else{
        uiotHBStr[0] = 0x02;
        uiotHBStr[1] = 0x00;
        uiotHBStr[2] = 0x00;
    }

    struct _zclReportAttrReportRec cliReportAttrRecord[3];
    cliReportAttrRecord[0].attrId = ZCL_ATTR_MODE_IDENTIFIER;
    cliReportAttrRecord[0].attrDataType = 0x42;
    cliReportAttrRecord[0].attrData = (uint8_t *)uiotNameStr;

    cliReportAttrRecord[1].attrId = ZCL_ATTR_SW_BUILD_ID;
    cliReportAttrRecord[1].attrDataType = 0x42;
    cliReportAttrRecord[1].attrData = (uint8_t *)uiotDateStr;

    cliReportAttrRecord[2].attrId = ZCL_MANUF_ATTR_UNISIOT_BASIC_HEARTBEAT;
    cliReportAttrRecord[2].attrDataType = 0x41;
    cliReportAttrRecord[2].attrData = (uint8_t *)uiotHBStr;


    zbRet_t reportRet = ZB_SUCC ;
   
    reportRet = zcl_sendReportAttr(&glb_zclTxParam, ZCL_CLUST_BASIC, 3, (struct _zclReportAttrReportRec *) &cliReportAttrRecord, NULL); 
    
    if(reportRet){
        printf("manually_send_a_report_online_to_zc ret 0x%x \r\n", reportRet);
    }


}


// mannually bind with zc and set configure report
zbRet_t zb_app_IeeeAddrRspCb(uint8_t status, uint16_t addr, union _zdpResp *respMessage)
{
    union _zbAddr dstAddr;
    
    if( (0 == status) && respMessage && (0 == respMessage->ieeeAddrResp.status) ){

        memcpy(dstAddr.ieeeAddr,  respMessage->ieeeAddrResp.ieeeAddrRemoteDev, sizeof(respMessage->ieeeAddrResp.ieeeAddrRemoteDev));

        if(zb_apsmeBindReq(ep1.ep, ZCL_CLUST_BASIC, ZB_APS_ADDR_MODE_IEEE, dstAddr, 0x01))
        {
            printf("%s, Fail to bind with GW\r\n", __func__);
            
            return ZB_ERR_FAIL;  
        }
        
        uint8_t reportChangeValue = 1;

        zcl_setLocalReport(ep1.ep, ZCL_CLUST_BASIC, ZCL_MANUF_ATTR_UNISIOT_BASIC_HEARTBEAT, ATTR_REPORT_MIN_INTVL,
                                    ATTR_REPORT_MAX_INTVL, &reportChangeValue, 0);

        return ZB_SUCC;

    }else{

        printf("%s, failed to get bind address \r\n", __func__);

        return ZB_ERR_FAIL;
    }
}

void  zb_app_startup_light_breath(TimerHandle_t p_timerhdl)
{

    static uint8_t breathLight = 0;

    if(0 == breathLight){
        demo_backlight_on(&ep1);
        breathLight = 1;
    }else{
        demo_backlight_off(&ep1);
        breathLight = 0;
    }
    
}

void  zb_app_startup_finish_handler(TimerHandle_t p_timerhdl)
{
    printf("%s ", __func__);

    /* stop all timer handler, ethier startup success or startup failed*/
  /*  if(p_timerhdl)
    {
        xTimerDelete(p_timerhdl, 0);
        p_timerhdl = NULL;
    }
*/
    zb_is_joining = false;

    demo_backlight_off(&ep1);

    if(zbStartBreathTimerHdl){
        xTimerDelete(zbStartBreathTimerHdl, 0);
        zbStartBreathTimerHdl = NULL;   
    }


    if(zbStartTimerHdl)
    {
     //   xTimerStop(zbStartTimerHdl, 0);
        xTimerDelete(zbStartTimerHdl, 0);
        zbStartTimerHdl = NULL;    
    }

    if(zbStartFinishTimerHdl){
        xTimerDelete(zbStartFinishTimerHdl, 0);
        zbStartFinishTimerHdl = NULL;
    }

}


void zb_startTimerHandler(TimerHandle_t p_timerhdl)
{
    uint8_t randomDelay = 0;
    uint8_t timeoutInSec = 0;
    
    if(p_timerhdl)
    {
        xTimerDelete(p_timerhdl, 0);
        zbStartTimerHdl = NULL;
    }
    if(!zb_isDevActiveInNwk())
    {
        if(!zcl_touchlinkTarInProgress())
        {
            printf("Do zb_start\r\n");
            zb_start();
        }
        else
        {
            printf("touchlink is ongoing\r\n");
            randomDelay = bl_rand()%3;
            timeoutInSec = ZB_START_INTVL_IN_SEC + randomDelay;
            zbStartTimerHdl = xTimerCreate("Timer", pdMS_TO_TICKS(timeoutInSec * 1000), 0, NULL,  zb_startTimerHandler);
            if(zbStartTimerHdl)
                xTimerStart(zbStartTimerHdl, 0);    
        }
    }
}

void zb_nwkStartupCompleteCb(const uint8_t status, const uint8_t roleType)
{
    uint8_t timeoutInSec = 0;
    uint8_t randomDelay = 0;

    if(status == 0x00)
    {
        /* stop timers: backlight breath, startup overtime, scan nwk  */
        zb_app_startup_finish_handler(zbStartTimerHdl);
        
        /* turn on backlight */
        demo_backlight_on(&ep1);

        /* set onoff attribute */
        uint8_t lightLoadStatus = demo_load_status(&ep1);
        zcl_setAttr(ZCL_CLUST_ONOFF, ZCL_ATTR_ONOFF_ONOFF, &lightLoadStatus, 1, ep1.ep); 
        if(lightLoadStatus){
            zb_app_load_on(&ep1);
        }else{
            zb_app_load_off(&ep1);
        }

        uint8_t bindCnt = 0;
        if(zb_getBindingTableCount(ep1.ep, ZCL_CLUST_BASIC, &bindCnt) && (!bindCnt)){
            
            /* delay a rand time to report basic cluster to zc */
            ep1ReportTimerHdl = xTimerCreate("reportTimer", pdMS_TO_TICKS(bl_rand()%9 + 10),  0, NULL,  manually_send_a_report_online_to_zc);
        
            if(ep1ReportTimerHdl)
                xTimerStart(ep1ReportTimerHdl, 0);  


           /* get zc ieee addr to to bind, and configure report */
            if( zb_isClustExist(ep1.ep, ZCL_CLUST_BASIC, ZCL_CLUST_DIR_IN))
            {
                uint16_t dstAddr = 0x0000;
                uint8_t zbRet = zb_zdpIeeeAddrReq(dstAddr, dstAddr, SINGLE_DEVICE_RESPONSE, 0x00,  zb_app_IeeeAddrRspCb);
                if(zbRet)
                    printf("GW address request failed %d \r\n", zbRet);
            } 
        }
    }
    else
    {
        if(appCb)
            printf("zb startup fail: 0x%x\r\n", status);
        else
        {
            printf("zb startup fail with status 0x%x\r\n", status);
            
            zb_has_joined_before = false;

            if(zb_is_joining){
                randomDelay = bl_rand()%3;
                timeoutInSec = ZB_START_INTVL_IN_SEC + randomDelay;

                //application layer can control the random interval between attempts, e.g.start a timer 
                zbStartTimerHdl = xTimerCreate("Timer", pdMS_TO_TICKS(timeoutInSec * 1000), 0, NULL,  zb_startTimerHandler);
                if(zbStartTimerHdl)
                    xTimerStart(zbStartTimerHdl, 0);

                //Build a timer to end NWK joining or rejoining     
                if(zbStartFinishTimerHdl == NULL){
                    zbStartFinishTimerHdl = xTimerCreate("FinishTimer", pdMS_TO_TICKS(ZED_START_FINISH_IN_SEC * 1000), 0, NULL, zb_app_startup_finish_handler);
                    if(zbStartFinishTimerHdl){
                        xTimerStart(zbStartFinishTimerHdl, 0);
                    }
                }
            }
        }
    }

    if(appCb)
        (appCb)(status, roleType);
}

void zb_app_leave()
{

    uint8_t device_ieee_addr[8]= {0,0,0,0,0,0,0,0};

    if(zb_nlmeLeave(device_ieee_addr, 0, 0)){
        printf("%s leave failed\r\n", __func__);
        return ;
    }

    zbStartBreathTimerHdl = xTimerCreate("ep1BreathTimer", pdMS_TO_TICKS(50), 1, NULL, zb_app_startup_light_breath);
    if(zbStartBreathTimerHdl)
        xTimerStart(zbStartBreathTimerHdl, 0);  

    BL702_Delay_MS(2000);

    // Reset all user flash except frame counter
    zb_resetToFactoryDefault();

    // Reboot
    GLB_SW_System_Reset();
    
}

void zb_nwkLeaveCb(const uint8_t status, const bool rejoin)
{
   // stop ota timer , if ota is ongoing
    if(otaTimerHdl) {
        xTimerDelete(otaTimerHdl, 0);
        otaTimerHdl = 0;
    }
    
    // stop all timer, if breath, scan is ongoing
    zb_app_startup_finish_handler(zbStartFinishTimerHdl);

    if(rejoin){
        //zb_start automatically when rejoin = 1, start a timer to end it 
        if(NULL == zbStartFinishTimerHdl){
            zbStartFinishTimerHdl = xTimerCreate("FinishTimer", pdMS_TO_TICKS(ZED_START_FINISH_IN_SEC * 1000), 0, NULL, zb_app_startup_finish_handler);
            if(zbStartFinishTimerHdl){
                xTimerStart(zbStartFinishTimerHdl, 0);
            }
        }
    }else{
        zb_app_leave();
    }
}


bool zb_apsIndCb(struct _zbApsdeDataIndication *apsInd)
{
    //printf("aps indication: srcAddr=%04x, srcEp=%d, dstAddr=%04x, dstEp=%d, profileId=%04x, clusterId=%04x\r\n", 
    //  apsInd->srcAddr.shortAddr, apsInd->srcEp, apsInd->dstAddr.shortAddr, apsInd->dstEp, apsInd->profileId, apsInd->clusterId);
        
    return true;
}

void zb_apsConfCb(struct _zbApsdeDataConfirm *apsConf)
{
    /*
    printf("aps confirm: status=0x%02x, dstAddr=%04x, dstEp=%d, srcEp=%d, tag=%lu\r\n", 
        apsConf->status, apsConf->dstAddr.shortAddr, apsConf->dstEp, apsConf->srcEp, apsConf->tag);
    */
}


/*
* Zigbee event callback handler
*/
zbRet_t zb_eventHandler(uint8_t evtId, uint8_t * evtParam)
{
    switch(evtId)
    {
        case ZB_EVT_STARTUP_COMPLETE:
        {
            zb_nwkStartupCompleteCb(evtParam[0], evtParam[1]);
            break;
        }
        case ZB_EVT_NWK_LEAVE:
        {
            zb_nwkLeaveCb(evtParam[0], evtParam[1]);
            break;
        }
        default:
            break;
    }

    return ZB_SUCC;
}

/*
* ZCL Callback
*/

/*
* evtParam[0] : OnOff value;
*/
zbRet_t zcl_onoffEventHandler(uint8_t ep, uint8_t evtId, uint8_t * evtParam)
{
    switch(evtId)
    {
        case ZCL_EVT_ONOFF_UPDATE_ONOFF:
        {
            // add PWM/LED driver code here
            if(evtParam[0] == 1)
            {
                if(zb_isJoined())
                {
                    if(ep == ep1.ep){
                        zb_app_load_on(&ep1);
                    }
                }
            }
            else
            {
                //zcl_getLocalAttrForPwm(NULL, NULL, &currHue, &currSat);
                if(zb_isJoined())
                {
                    if(ep == ep1.ep){ 
                        zb_app_load_off(&ep1);
                    }
                }
            }
            break;
        }
    }

    return ZB_SUCC;
}

/*
* evtParam[0] : CurrentLevel value;
*/
zbRet_t zcl_levelControlEventHandler(uint8_t ep, uint8_t evtId, uint8_t * evtParam)
{
    #if 0
    switch(evtId)
    {
        case ZCL_EVT_LEVEL_UPDATE_CURR_LEVEL:
        {
            // add PWM/LED driver code here
            printf("Current Level is 0x%x\r\n", evtParam[0]);
            uint8_t onoff = 0;
            uint8_t currHue = 0;
            uint8_t currSat = 0;
            zcl_getLocalAttrForPwm(&onoff, NULL, &currHue, &currSat);
            if((onoff != 0) && zb_isJoined())
            {
                set_color(evtParam[0], currHue, currSat);
            }
            else if((onoff == 0) && zb_isJoined())
            {
                // set PWM off
                set_color(0, 0, 0);
            }
            break;
        }
        case ZCL_EVT_CLIENT_CMD_RECEIVED:
        {
            printf("Level Control Command Received is: 0x%x\r\n", evtParam[0]);
            printf("Payload length: %d\r\n", evtParam[1]);
            for(int i=0; i<evtParam[1]; i++)
            {
                printf("Payload[%d]:0x%x\r\n", i, evtParam[i+2]);
            }
            break;
        }
    }

    #endif
    return ZB_SUCC;   

}

zbRet_t zcl_scenesEventHandler(uint8_t ep, uint8_t evtId, uint8_t * evtParam)
{
    switch(evtId)
    {

        case ZCL_EVT_CLIENT_CMD_RECEIVED:
        {
            printf("Scenes Command Received is: 0x%x\r\n", evtParam[0]);
            printf("Payload length: %d\r\n", evtParam[1]);
            for(int i=0; i<evtParam[1]; i++)
            {
                printf("Payload[%d]:0x%x\r\n", i, evtParam[i+2]);
            }
            break;
        }
    }

    return ZB_SUCC;    
}

/*
* evtParam[0-1] : identify time value; 
*/

zbRet_t zcl_IdentifyEventHandler(uint8_t ep, uint8_t evtId, uint8_t * evtParam)
{
    switch(evtId)
    {
        case ZCL_EVT_IDENTIFY_UPDATE_IDENTIFY_TIME:
        {
            // add customer application code here
            uint16_t identifyTime = (uint16_t)evtParam[0] | (uint16_t)(evtParam[1]<<8);
            printf("identify time is %d\r\n", identifyTime);
            break;
        }

        case ZCL_EVT_CLIENT_CMD_RECEIVED:
        {
            printf("Identify Cluster Command Received is: 0x%x\r\n", evtParam[0]);
            printf("Payload length: %d\r\n", evtParam[1]);
            for(int i=0; i<evtParam[1]; i++)
            {
                printf("Payload[%d]:0x%x\r\n", i, evtParam[i+2]);
            }
            break;
        }
    }

    return ZB_SUCC;    
}

zbRet_t zcl_colorControlEventHandler(uint8_t ep, uint8_t evtId, uint8_t * evtParam)
{
    printf("zcl_colorControlEventHandler  evtId: 0x%02x\r\n", evtId);
    #if 0
    uint8_t onoff = 0;
    uint8_t currLevel = 0;
    uint8_t currHue = 0, currSat = 0; 

    switch(evtId)
    {
        case ZCL_EVT_COLOR_UPDATE_CURRENT_HUE:
        {
            currHue = (uint8_t)evtParam[0];
            printf("currentHue is: 0x%02x\r\n", currHue);

            zcl_getLocalAttrForPwm(&onoff, &currLevel, NULL, &currSat); 
            if((onoff != 0) && zb_isJoined())
            {
                set_color(currLevel, currHue, currSat);
            }
        break;
        }

        case ZCL_EVT_COLOR_UPDATE_CURRENT_SATURATION:
        {
            currSat = (uint8_t)evtParam[0];
            printf("currentSaturation is: 0x%02x\r\n", currSat);
            zcl_getLocalAttrForPwm(&onoff, &currLevel, &currHue, NULL); 
            if((onoff != 0) && zb_isJoined())
            {
                set_color(currLevel, currHue, currSat);
            } 
        break;
        }

        case ZCL_EVT_COLOR_UPDATE_CURRENT_X:
        {
            uint16_t currentX = (uint16_t)evtParam[0] | (uint16_t)(evtParam[1]<<8);
            printf("currentX is: 0x%04x\r\n", currentX);
        break;
        }

        case ZCL_EVT_COLOR_UPDATE_CURRENT_Y:
        {
            uint16_t currentY = (uint16_t)evtParam[0] | (uint16_t)(evtParam[1]<<8);
            printf("currentY is: 0x%04x\r\n", currentY);
        break;
        }

        case ZCL_EVT_COLOR_UPDATE_TEMP_MIREDS:
        {
            uint16_t colorTempMireds = (uint16_t)evtParam[0] | (uint16_t)(evtParam[1]<<8);
            printf("colorTempMireds is: 0x%04x\r\n", colorTempMireds);
        break;
        }

        case ZCL_EVT_COLOR_UPDATE_ENHANCEDCURRENT_HUE:
        {
            uint16_t enhancedCurrentHue = (uint16_t)evtParam[0] | (uint16_t)(evtParam[1]<<8);
            printf("enhancedCurrentHue is: 0x%04x\r\n", enhancedCurrentHue);
        break;
        }
        case ZCL_EVT_CLIENT_CMD_RECEIVED:
        {
            printf("Color Control Command Received is: 0x%x\r\n", evtParam[0]);
            printf("Payload length: %d\r\n", evtParam[1]);
            for(int i=0; i<evtParam[1]; i++)
            {
                printf("Payload[%d]:0x%x\r\n", i, evtParam[i+2]);
            }
            break;
        }

        default: break;
    }

#endif
    return ZB_SUCC;    
}


void  zcl_checkOtaProgress(TimerHandle_t p_timerhdl)
{
    uint32_t percentage = 0;
    uint32_t currOffset = 0;
    uint32_t imageSize = 0;
    zcl_otaGetProgress(otaClientEp, &currOffset, &imageSize, &percentage);
    printf("OTA Progress[%d][%ld%%]: %ld / %ld\r\n", otaClientEp, percentage, currOffset, imageSize);
    xTimerStart(p_timerhdl, 0);
}

zbRet_t zcl_otaEventHandler(uint8_t ep, uint8_t evtId, uint8_t * evtParam)
{
    printf("%s, evtId %d, ep %d\r\n", __func__,evtId, ep);
    switch(evtId)
    {
        case ZCL_EVT_OTA_START:
        {
            otaClientEp = ep;
            otaTimerHdl = xTimerCreate("Timer", pdMS_TO_TICKS(ZCL_OTA_GET_PROGRESS_INTVL_IN_SEC * 1000), 0, NULL,  zcl_checkOtaProgress);
            if(otaTimerHdl)
                xTimerStart(otaTimerHdl, 0);
        }
        break;
   
        case ZCL_EVT_OTA_ABORT:
        {
            if(otaTimerHdl) {
                xTimerDelete(otaTimerHdl, 0);
                otaTimerHdl = 0;
            }
        }
        break;
		case ZCL_EVT_OTA_CHECK_SIGNATURE_SIGNER:
        {
            printf("signature signer IEEE address:");
            for(uint8_t i=0;i<8;i++)
            {
                printf("%02x", evtParam[i]);
            }
            printf("\r\n");
        }
        break;
        case ZCL_EVT_OTA_CHECK_CERTIFICATE_ISSUER:
        {
            printf("certificate issuer IEEE address:");
            for(uint8_t i=0;i<8;i++)
            {
                printf("%02x", evtParam[i]);
            }
            printf("\r\n");
        }
        break;
   
        case ZCL_EVT_OTA_IMAGE_CMPLT:
        { 
            uint32_t percentage = 0;
            uint32_t currOffset = 0;
            uint32_t imageSize = 0;
            zcl_otaGetProgress(otaClientEp, &currOffset, &imageSize, &percentage);
            printf("OTA Progress[%d][%ld%%]: %ld / %ld\r\n", otaClientEp, percentage, currOffset, imageSize);
            if(evtParam[0] == ZCL_OTA_STATUS_SUCC)
                printf("New image reception is completed successfully\r\n");
            else if(evtParam[0] == ZCL_OTA_STATUS_IMG_VERIFY_FAILED)
                printf("New image is not valid because of verification failure\r\n");

            if(otaTimerHdl) {
                xTimerDelete(otaTimerHdl, 0);
                otaTimerHdl = 0;
            }

            //stack will report ZCL_EVT_OTA_NEW_IMAGE_APPLY event to reboot device 
        }
        break;
   
        case ZCL_EVT_OTA_NEW_IMAGE_APPLY:
        {
            printf("To apply the new image\r\n");
            BL702_Delay_MS(10);
            hal_reboot();
        }
        break;

        default:
            break;
    }

    return ZB_SUCC; 
}

zbRet_t zcl_touchlinkEventHandler(uint8_t ep, uint8_t evtId, uint8_t * evtParam)
{
    switch(evtId)
    {
        case ZCL_EVT_TL_ON_IDENTIFY:
        {
            uint16_t dur;
            memcpy(&dur, evtParam, 2);
            printf("Identify duration is: %d\r\n", dur);
        }
        break;
        case ZCL_EVT_TL_INI_RESULT:
        {
            printf("Status of result is: 0x%x\r\n", evtParam[0]);
            break;
        }
        default:
        break;
    }

    return ZB_SUCC;    
}

void zcl_touchlinkIniScanRespHandler(uint8_t ep, struct _zclTlScanResp * scanResp[], uint8_t num)
{
    uint64_t targetExtAddr = 0;

    printf("the number of received scan response is %d\r\n", num);
    if(!num)
        return;

    for(int i = 0; i < num; i++)
    {
        memcpy(&targetExtAddr, (uint8_t *)scanResp[i] + sizeof(struct _zclTlScanResp), sizeof(uint64_t));
        printf("target[%d]:0x%llx \r\n", i, targetExtAddr); 
    }
    //for example, take target 0
    zcl_touchlinkIniChooseTarget(ep, 0);
}


void zcl_unisiotPassThroughEventHandler(uint8_t ep, uint8_t evtId, uint8_t * evtParam)
{
    uint8_t attrData[50] = {0};
    uint8_t dataSize = 0;

    switch(evtId)
    {
        case ZCL_EVT_UPDATE_ATTRIBUTE:
        {
            uint16_t attrId = evtParam[0] + (evtParam[1] << 8);
            zcl_getAttr(ZCL_MANUF_CLUST_UNISIOT_PASS_THROUGH, attrId, attrData, &dataSize, ep);  // zigbee_app  ep is 1; 

            printf("%s: attribute 0x%04x update , ", __func__, attrId);
            for (uint8_t i = 0; i < dataSize; i++)
            {
                printf("zcl get data: 0x%x\r\n", attrData[i]);
            }
            printf("\r\n");

        }
        break;
        default:
        break;
    }
     
}

void zcl_unisiotCertificationEventHandler(uint8_t ep, uint8_t evtId, uint8_t * evtParam)
{
    uint8_t attrData[50] = {0};
    uint8_t dataSize = 0;

    switch(evtId){
        case ZCL_EVT_UPDATE_ATTRIBUTE:
        {
            uint16_t attrId = evtParam[0] + (evtParam[1] << 8);
            zcl_getAttr(ZCL_MANUF_CLUST_UNISIOT_CERTIFICATION, attrId, attrData, &dataSize, ep);  // zigbee_app  ep is 1; 
            printf("%s: attribute 0x%04x update , ", __func__, attrId);
            for (uint8_t i = 0; i < dataSize; i++)
            {
                printf("zcl get data: 0x%x\r\n", attrData[i]);
            }
            printf("\r\n");
        }
        break;
        default:
        break;
    }
}

void zcl_unisiotSceneEventHandler(uint8_t ep, uint8_t evtId, uint8_t * evtParam)
{
    uint8_t attrData[50] = {0};
    uint8_t dataSize = 0;

    switch(evtId){
        case ZCL_EVT_UPDATE_ATTRIBUTE:
        {
            uint16_t attrId = evtParam[0] + (evtParam[1] << 8);
            zcl_getAttr(ZCL_MANUF_CLUST_UNISIOT_SCENE, attrId, attrData, &dataSize, ep);  // zigbee_app  ep is 1; 
            printf("%s: attribute 0x%04x update , ", __func__, attrId);
            for (uint8_t i = 0; i < dataSize; i++)
            {
                printf("zcl get data: 0x%x\r\n", attrData[i]);
            }
            printf("\r\n");
        }
        break;
        default:
        break;
    }
}

#ifdef EASYFLASH_ENABLE
void rebootCheck_startTimerHandler(TimerHandle_t p_timerhdl)
{
   // uint8_t randomDelay = 0;
   // uint8_t timeoutInSec = 0;
    
    if(p_timerhdl)
    {
        xTimerDelete(p_timerhdl, 0);
        deviceRebootCheckHdl = NULL;
    }

    uint32_t i_boot_times = 0;
   // char *c_old_boot_times;
    char c_new_boot_times[11] = {0}; 
    // Recover Default Value
    i_boot_times = 3;
    /* interger to string */
    sprintf(c_new_boot_times,"%ld", i_boot_times);
    /* set and store the boot count number to Env */
    ef_set_env("boot_times", c_new_boot_times);
    ef_save_env();    
}

void start_reboot_check_timer(void)
{
    //application layer can control the interval between attempts, e.g.start a timer 
    deviceRebootCheckHdl = xTimerCreate("Timer", pdMS_TO_TICKS(REBOOT_CHECK_IN_SEC * 1000), 
                                        0, NULL,  rebootCheck_startTimerHandler);
    if(deviceRebootCheckHdl)
        xTimerStart(deviceRebootCheckHdl, 0); 
}
void reboot_check(void)
{
    uint32_t i_boot_times = 0;
    char *c_old_boot_times, c_new_boot_times[11] = {0};

    /* get the boot count number from Env */
    c_old_boot_times = ef_get_env("boot_times");

    i_boot_times = atol(c_old_boot_times);

    // i_boot_times > 1
    // default value is 3
    // reboot first time: i_boot_times is still 3
    // reboot second time: i_boot_times is 2
    // reboot third time: i_boot_times is 1
    // define reboot 3 times to reset to factory
    if(i_boot_times > 1)
    {
        /* boot count -1 */
        i_boot_times --;
        printf("The system now boot 0x%lx times\n", i_boot_times);
        /* interger to string */
        sprintf(c_new_boot_times,"%ld", i_boot_times);
        /* set and store the boot count number to Env */
        ef_set_env("boot_times", c_new_boot_times);
        ef_save_env();  

        // Start timer
        start_reboot_check_timer();
    }
    else
    {
        // Recover Default Value
        i_boot_times = 3;
        /* interger to string */
        sprintf(c_new_boot_times,"%ld", i_boot_times);
        printf("\r\nc_new_boot_times is %s\r\n",c_new_boot_times);
        /* set and store the boot count number to Env */
        ef_set_env("boot_times", c_new_boot_times);
        ef_save_env();

        // Remove itself
        uint8_t device_ieee_addr[8]= {0,0,0,0,0,0,0,0};
        zbRet_t ret = ZB_SUCC; 
        ret = zb_nlmeLeave(device_ieee_addr, 0, 0);
        if(ret)
            printf("%s, failed with status 0x%x\r\n", __func__, ret);
        
        // Reset all user flash except frame counter
        zb_resetToFactoryDefault();

        // Reboot
        GLB_SW_System_Reset();
    }
}
#endif

static void zb_regCommonCb(void)

{
    // register zb event;
    zb_registerCb(zb_eventHandler);

    // register aps indication callback
    //zb_registerApsIndCb(zb_apsIndCb);

    // register aps confirm callback
    //zb_registerApsConfCb(zb_apsConfCb);
}

static void zcl_regClsCb(void)
{
    // register zcl cluster callback
    zcl_registerClustCb(ZCL_CLUST_ONOFF, zcl_onoffEventHandler);
   // zcl_registerClustCb(ZCL_CLUST_LEVEL_CTRL, zcl_levelControlEventHandler);
    zcl_registerClustCb(ZCL_CLUST_IDENTIFY, zcl_IdentifyEventHandler);
   // zcl_registerClustCb(ZCL_CLUST_COLOR_CTRL, zcl_colorControlEventHandler);
    zcl_registerClustCb(ZCL_CLUST_OTA, zcl_otaEventHandler);
    zcl_registerClustCb(ZCL_CLUST_SCENES, zcl_scenesEventHandler);
    zcl_registerClustCb(ZCL_CLUST_TL_COMMS, zcl_touchlinkEventHandler);


    zcl_registerClustCb(ZCL_MANUF_CLUST_UNISIOT_PASS_THROUGH, zcl_unisiotPassThroughEventHandler);
    zcl_registerClustCb(ZCL_MANUF_CLUST_UNISIOT_CERTIFICATION, zcl_unisiotCertificationEventHandler);
    zcl_registerClustCb(ZCL_MANUF_CLUST_UNISIOT_SCENE, zcl_unisiotSceneEventHandler);


}

// General Register Zigbee Callbacks
void register_zb_cb(void)
{
    zb_regCommonCb();

    zcl_regClsCb();

    zcl_touchlinkIniRegScanRespHandler(zcl_touchlinkIniScanRespHandler);
}

void zb_register_app_cb(zb_appCb cb)
{
    appCb = cb;
}


void zb_app_scan_trigger()
{
    //mannual trigger for new device to startup zigbee nwk
    if (!zb_isJoined()){
        zb_is_joining = true;
        zb_start();

        zbStartBreathTimerHdl = xTimerCreate("ep1BreathTimer", pdMS_TO_TICKS(50), 1, NULL, zb_app_startup_light_breath);
        if(zbStartBreathTimerHdl)
            xTimerStart(zbStartBreathTimerHdl, 0);  
    }
}

void zb_app_startup(void)
{
    if(!ep1.ep){
        printf("%s Invalid endpoint \r\n", __func__);
        return ;
    }

    uint16_t profile_id = 0x0104;
    struct _deviceFlags deviceFlags = {0};

    uint16_t device_id = 0x0100;
    zbRet_t status;

    deviceFlags.touchlinkTarget = 1;
    
    status = zb_registerDevice(ep1.ep, profile_id, device_id, deviceFlags);
    printf("Dynamic register ep1.ep1 status is 0x%x\r\n", status);
    zcl_basicRegisterServer(ep1.ep);
    zcl_onOffRegisterServer(ep1.ep);
    zcl_identifyRegisterServer(ep1.ep);
    zcl_groupsRegisterServer(ep1.ep);
    zcl_powerCfgRegisterServer(ep1.ep);
    zcl_iasZoneRegisterServer(ep1.ep, IAS_ZONE_TYPE_STANDARD_CIE);
  // zcl_scenesRegisterServer(ep1.ep);
  //  zcl_levelRegisterServer(ep1.ep);
  //  zcl_colorRegisterServer(ep1.ep);
    zcl_touchlinkRegisterServer(ep1.ep);
    zcl_otaRegisterClient(ep1.ep);
    zcl_otaAutoLocateOtaServerEnable(false);

    zcl_unisiotPassThroughRegisterServer(ep1.ep);
    zcl_unisiotCertificationRegisterServer(ep1.ep);
    zcl_unisiotSceneRegisterServer(ep1.ep);

    zcl_windowCoveringRegisterServer(ep1.ep);

    zb_initDevices();


    //set basic attribute
    char uiotNameStr[13] = {12, 'U', 'I', 'O', 'T', '-', '[', '0', '2', ',', '0', '1',']'};
    char uiotDateStr[16] = {15, 'V', '4', '.', '8', '0', '-', '8', '.', '0', '0', '-','V','5','.','0'};
    char uiotHBStr[] = {2, 0x01, 0x00};
    
    zcl_setAttr(ZCL_CLUST_BASIC, ZCL_ATTR_MODE_IDENTIFIER, (uint8_t *)uiotNameStr, sizeof(uiotNameStr), ep1.ep); 
    zcl_setAttr(ZCL_CLUST_BASIC, ZCL_ATTR_SW_BUILD_ID, (uint8_t *)uiotDateStr, sizeof(uiotDateStr), ep1.ep); 
    zcl_setAttr(ZCL_CLUST_BASIC, ZCL_MANUF_ATTR_UNISIOT_BASIC_HEARTBEAT, (uint8_t *)uiotHBStr, sizeof(uiotHBStr), ep1.ep); 


    //set new file version before make ota firmware
    uint32_t currentFileVersion = OTA_FILE_VERSION;
    zcl_setClientAttr(ZCL_CLUST_OTA, ZCL_ATTR_CURRENT_FILE_VERSION, (uint8_t*)&currentFileVersion, 4, ep1.ep);
    uint16_t imageType = OTA_IMAGE_TYPE;
    zcl_setClientAttr(ZCL_CLUST_OTA, ZCL_ATTR_DEVICE_IMAGE_TYPE_ID, (uint8_t*)&imageType, 2, ep1.ep);

    if (!zb_isJoined())
    {
        zb_setRole(ZB_ROLE_ROUTER);
    }else{
        zb_start();
    } 

}

//#endif
