#  Demo Light Description
## Build
   ./genzrstartup

# Notice
   This demo code configure 1 endpoints and 3 GPIOs as 1 gang light. Every gang light has 1 onoff button , 1 backlight and 1 lighting load. We suppose these GPIOs are able to use, and all behavior and performance descriptions are based on this suppose. If you need practical effect, please configure GPIOs based on your peripheral circuit.

## Function Description
     
   1. Power up the BL702/706 device, short press  ( < 3s ) button will turn over the lighting loads status.
 
   2. Long press button 1 ( < 8s ), device will start as ZR and search the zigbee network in 1mins, backlight will breath at same time.
    
   3. Long Long press button 1 ( > 8s), device will leave nwk in 2 seconds, backlight will breath at same time.
    
   After joined the network:

   1. Support Basic Cluster, endpoint1 will send attributes (ModelIdentifier/SWBuildID/HeartBeat) to ZC automatically, when the ZR join nwk success.
   
   2. Support Basic Cluster, endpoint will automatical bind with ZC, and send HeartBeat periodically.
    
   3. Support On/Off server, receive ZCL client commands will set/toggle Lighting load.
   
   4. Support Lighting load status synchronize with HeartBeat attribute,  i.e. press button or receive command to change the On/Off, the HearBeat attribute will report to ZC.
 
   

